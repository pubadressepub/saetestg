-- Insertion de données dans la table Utilisateur
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (1, 'Dupont', 'Jean', '1234567890', '123 Rue Principale', '31000', 'Toulouse', 'mdp1', '2023-01-01', '192.168.1.1');
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (2, 'Martin', 'Marie', '9876543210', '456 Avenue des Fleurs', '73000', 'Paris', 'mdp2', '2023-01-02', '192.168.1.2');
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (3, 'Lefebvre', 'Sophie', '5551234567', '789 Rue du Commerce', '59000', 'Lille', 'mdp3', '2023-01-03', '192.168.1.3');
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (7, 'Martinez', 'Luc', '2223334444', '654 Rue de la Paix', '31000', 'Toulouse', 'mdp7', '2023-01-07', '192.168.1.7');
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (4, 'Dubois', 'Pierre', '1112223333', '321 Rue Saint-Jacques', '69000', 'Lyon', 'mdp4', '2023-01-04', '192.168.1.4');
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (5, 'Garcia', 'Julie', '4445556666', '789 Boulevard de la Mer', '75000', 'Paris', 'mdp5', '2023-01-05', '192.168.1.5');
INSERT INTO Utilisateur (id_utilisateur, Nom, Prenom, Telephone, Adresse, Code_postal, Ville, mdp, date_connexion, ip) VALUES (6, 'Thomas', 'Camille', '7778889999', '987 Avenue des Champs', '59000', 'Lille', 'mdp6', '2023-01-06', '192.168.1.6');

-- Insertion de données dans la table Creneau
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Généraliste 1', '2023-11-13', '09:00:00', '09:30:00', 10, 5, '00:30:00', 1);
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Généraliste 2', '2023-11-13', '09:00:00', '09:30:00', 10, 5, '00:30:00', 2);
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Généraliste 4', '2023-11-16', '09:00:00', '09:30:00', 10, 5, '00:30:00', 3);
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Spécialiste 2', '2023-11-17', '14:30:00', '15:00:00', 5, 2, '00:30:00', 5);
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Spécialiste 1', '2023-11-17', '14:30:00', '15:00:00', 5, 2, '00:30:00', 4);
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Radiologie 1', '2023-11-15', '11:15:00', '11:45:00', 15, 8, '00:30:00', 6);
INSERT INTO Creneau (Description, Date_Creneau, Heure_Debut, Heure_Fin, Nb_max, nombrcle_de_personne, Interval_Creneau, id_utilisateur) VALUES ('RDV Radiologie 2', '2023-11-14', '11:15:00', '11:45:00', 15, 8, '00:30:00', 7);
                    
-- Insertions dans la table Permission
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 1);
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 2);
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 3);
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 4);
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 5);
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 6);
INSERT INTO Permission (statut, id_utilisateur) VALUES ('Patient', 7);



