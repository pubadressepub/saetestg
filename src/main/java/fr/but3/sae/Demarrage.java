package fr.but3.sae;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import fr.but3.sae.repository.PermissionRepository;
import fr.but3.sae.repository.UtilisateurRepository;
import fr.but3.sae.repository.CreneauRepository;

@Component
public class Demarrage implements ApplicationRunner {

    @Autowired
    private PermissionRepository permissionsRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Autowired
    private CreneauRepository creneauRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        lireEtAfficherDonnees();
    }

    private void lireEtAfficherDonnees() {
        System.out.println("Contenu de la table Permission:");
        permissionsRepository.findAll().forEach(permissions -> System.out.println(permissions));

        System.out.println("Contenu de la table Utilisateur:");
        utilisateurRepository.findAll().forEach(utilisateur -> System.out.println(utilisateur));

        System.out.println("Contenu de la table Creneau:");
        creneauRepository.findAll().forEach(creneau -> System.out.println(creneau));
    }
}
