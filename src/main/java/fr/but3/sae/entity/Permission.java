package fr.but3.sae.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "Permission")
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_permission;
    private String statut;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur") 
    private Utilisateur utilisateur; 
}
