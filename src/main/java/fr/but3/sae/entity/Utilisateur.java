package fr.but3.sae.entity;

import jakarta.persistence.*;
import lombok.Data;
import java.util.Set;

@Entity
@Data
@Table(name = "Utilisateur")
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_utilisateur;
    private String nom;
    private String prenom;
    private String telephone;
    private String adresse;
    private String code_postal;
    private String ville;
    private String mdp;
    private String date_connexion;
    private String ip;

}
