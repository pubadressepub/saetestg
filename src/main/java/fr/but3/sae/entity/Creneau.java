package fr.but3.sae.entity;

import jakarta.persistence.*;
import lombok.Data;
import java.sql.Date;
import java.sql.Time;

@Entity
@Data
@Table(name = "Creneau")
public class Creneau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_creneau;
    private String description;
    private Date date_creneau;
    private Time heure_debut;
    private Time heure_fin;
    private int nb_max;
    private int nombrcle_de_personne;
    private Time interval_creneau;

    @ManyToOne
    @JoinColumn(name = "id_utilisateur")
    private Utilisateur utilisateur;
}
