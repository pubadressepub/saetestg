package fr.but3.sae.repository;

import org.springframework.data.repository.CrudRepository;
import fr.but3.sae.entity.Utilisateur;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Integer> {
}
