package fr.but3.sae.repository;

import org.springframework.data.repository.CrudRepository;
import fr.but3.sae.entity.Permission;

public interface PermissionRepository extends CrudRepository<Permission, Integer> {
}
