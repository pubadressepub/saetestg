package fr.but3.sae.repository;

import org.springframework.data.repository.CrudRepository;
import fr.but3.sae.entity.Creneau;

public interface CreneauRepository extends CrudRepository<Creneau, Integer> {
}
